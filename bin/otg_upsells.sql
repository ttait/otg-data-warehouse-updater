#######  CREATE necessary tables in the database
DROP TABLE IF EXISTS otg_upsells;
CREATE TABLE `otg_upsells` (
  `menu_item_id` decimal(18,0) NOT NULL,
  `upsell_item_id` decimal(18,0) NOT NULL,
  `item_title` varchar(255) NOT NULL,
  `item_description` varchar(255) NOT NULL,
  `item_image` varchar(255) NOT NULL,
  `item_detail_image` varchar(255) NOT NULL,
  `item_detail_image_landscape` varchar(255) NOT NULL,
  `item_price` decimal(10,2) DEFAULT '0.00',
  `item_count` int,
  `item_modifiers` int,
  `item_restrictions` int,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `title_zhh` varchar(100) DEFAULT NULL,
  `description_zhh` varchar(720) DEFAULT NULL,
  `title_fra` varchar(100) DEFAULT NULL,
  `description_fra` varchar(720) DEFAULT NULL,
  `title_deu` varchar(100) DEFAULT NULL,
  `description_deu` varchar(720) DEFAULT NULL,
  `title_ell` varchar(100) DEFAULT NULL,
  `description_ell` varchar(720) DEFAULT NULL,
  `title_heb` varchar(100) DEFAULT NULL,
  `description_heb` varchar(720) DEFAULT NULL,
  `title_hin` varchar(100) DEFAULT NULL,
  `description_hin` varchar(720) DEFAULT NULL,
  `title_ita` varchar(100) DEFAULT NULL,
  `description_ita` varchar(720) DEFAULT NULL,
  `title_jpn` varchar(100) DEFAULT NULL,
  `description_jpn` varchar(720) DEFAULT NULL,
  `title_kor` varchar(100) DEFAULT NULL,
  `description_kor` varchar(720) DEFAULT NULL,
  `title_spa` varchar(100) DEFAULT NULL,
  `description_spa` varchar(720) DEFAULT NULL,
  `title_pol` varchar(100) DEFAULT NULL,
  `description_pol` varchar(720) DEFAULT NULL,
  `title_rus` varchar(100) DEFAULT NULL,
  `description_rus` varchar(720) DEFAULT NULL,
  `title_tur` varchar(100) DEFAULT NULL,
  `description_tur` varchar(720) DEFAULT NULL,
  `title_ron` varchar(100) DEFAULT NULL,
  `description_ron` varchar(720) DEFAULT NULL,
  `title_dan` varchar(100) DEFAULT NULL,
  `description_dan` varchar(720) DEFAULT NULL,
  `title_fin` varchar(100) DEFAULT NULL,
  `description_fin` varchar(720) DEFAULT NULL,
  `title_ces` varchar(100) DEFAULT NULL,
  `description_ces` varchar(720) DEFAULT NULL,
  `title_hun` varchar(100) DEFAULT NULL,
  `description_hun` varchar(720) DEFAULT NULL,
  `title_ara` varchar(100) DEFAULT NULL,
  `description_ara` varchar(720) DEFAULT NULL,
  PRIMARY KEY (`menu_item_id`,`upsell_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=16;

DROP TABLE IF EXISTS tmp_dw_upsell_order_items;
CREATE TABLE tmp_dw_upsell_order_items (
	order_id varchar (50), 
	order_item_id varchar (50), 
	menu_item_id int, 
	item_title varchar (50), 
	order_date datetime, 
	item_quantity int, 
	menu_id int,
	PRIMARY KEY (`menu_item_id`,order_item_id, item_title)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS tmp_otg_upsells;
CREATE TABLE `tmp_otg_upsells` (
  `menu_item_id` decimal(18,0) NOT NULL,
  `upsell_item_id` decimal(18,0) NOT NULL,
  `item_title` varchar(255) NOT NULL,
  `item_description` varchar(255) NOT NULL,
  `item_image` varchar(255) NOT NULL,
  `item_detail_image` varchar(255) NOT NULL,
  `item_detail_image_landscape` varchar(255) NOT NULL,
  `item_price` decimal(10,2) DEFAULT '0.00',
  `item_count` int,
  `item_modifiers` int,
  `item_restrictions` int,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title_zhh` varchar(100) DEFAULT NULL,
  `description_zhh` varchar(720) DEFAULT NULL,
  `title_fra` varchar(100) DEFAULT NULL,
  `description_fra` varchar(720) DEFAULT NULL,
  `title_deu` varchar(100) DEFAULT NULL,
  `description_deu` varchar(720) DEFAULT NULL,
  `title_ell` varchar(100) DEFAULT NULL,
  `description_ell` varchar(720) DEFAULT NULL,
  `title_heb` varchar(100) DEFAULT NULL,
  `description_heb` varchar(720) DEFAULT NULL,
  `title_hin` varchar(100) DEFAULT NULL,
  `description_hin` varchar(720) DEFAULT NULL,
  `title_ita` varchar(100) DEFAULT NULL,
  `description_ita` varchar(720) DEFAULT NULL,
  `title_jpn` varchar(100) DEFAULT NULL,
  `description_jpn` varchar(720) DEFAULT NULL,
  `title_kor` varchar(100) DEFAULT NULL,
  `description_kor` varchar(720) DEFAULT NULL,
  `title_spa` varchar(100) DEFAULT NULL,
  `description_spa` varchar(720) DEFAULT NULL,
  `title_pol` varchar(100) DEFAULT NULL,
  `description_pol` varchar(720) DEFAULT NULL,
  `title_rus` varchar(100) DEFAULT NULL,
  `description_rus` varchar(720) DEFAULT NULL,
  `title_tur` varchar(100) DEFAULT NULL,
  `description_tur` varchar(720) DEFAULT NULL,
  `title_ron` varchar(100) DEFAULT NULL,
  `description_ron` varchar(720) DEFAULT NULL,
  `title_dan` varchar(100) DEFAULT NULL,
  `description_dan` varchar(720) DEFAULT NULL,
  `title_fin` varchar(100) DEFAULT NULL,
  `description_fin` varchar(720) DEFAULT NULL,
  `title_ces` varchar(100) DEFAULT NULL,
  `description_ces` varchar(720) DEFAULT NULL,
  `title_hun` varchar(100) DEFAULT NULL,
  `description_hun` varchar(720) DEFAULT NULL,
  `title_ara` varchar(100) DEFAULT NULL,
  `description_ara` varchar(720) DEFAULT NULL,
  PRIMARY KEY (`menu_item_id`,`upsell_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=16;

DROP TABLE IF EXISTS tmp_manual_upsell_items;
CREATE TABLE tmp_manual_upsell_items (
	menu_item_id int,
	related_to_menu_item_id int,
	PRIMARY KEY(menu_item_id, related_to_menu_item_id)
)  ENGINE=InnoDB DEFAULT CHARSET=latin1;



####	procedure to setup the temp table that will house the menu_items for a particular restaurant, beginning at the start date
DROP PROCEDURE IF EXISTS sp_setup_upsell_table;
DELIMITER //
CREATE PROCEDURE sp_setup_upsell_table(RESTID INT, STARTDATE DATE)
BEGIN

	TRUNCATE TABLE tmp_dw_upsell_order_items;

	INSERT INTO tmp_dw_upsell_order_items
	SELECT dw.order_id, dw.order_item_id, dw.menu_item_id, dw.item_title, dw.order_date, dw.item_quantity, oi.menu_id
	FROM dw_order_items dw
	JOIN menu_items oi on dw.menu_item_id = oi.id
	WHERE dw.restaurant_id = RESTID
	AND dw.order_date BETWEEN STARTDATE AND NOW();
	
	SELECT DISTINCT menu_item_id 
	FROM tmp_dw_upsell_order_items;
END//
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_update_upsells;
DELIMITER //
CREATE PROCEDURE sp_update_upsells(ITEMID int)
BEGIN

	TRUNCATE TABLE tmp_otg_upsells;
	
	INSERT INTO tmp_otg_upsells
	SELECT dw.menu_item_id, 
		dwoi.menu_item_id AS upsell_item_id, 
		dwoi.item_title, mi.description, 
		concat("/images/media/media_",thumb.media_id,".jpg") AS imgUrl,
		concat("/images/media/media_",land.media_id,".jpg") AS detailImageLandscape,
		concat("/images/media/media_",portrait.media_id,".jpg") AS detailImage, 
		mic.price AS item_price, 
		sum(dwoi.item_quantity) AS item_count, 
		mi.modifier_collection_id,
		mi.restrictions,
		now() as create_datetime,
		mi.`title_zhh`,mi.`description_zhh`,mi.`title_fra`,mi.`description_fra`,mi.`title_deu`,mi.`description_deu`,mi.`title_ell`,mi.`description_ell`,mi.`title_heb`,
		mi.`description_heb`,mi.`title_hin`,mi.`description_hin`,mi.`title_ita`,mi.`description_ita`,mi.`title_jpn`,mi.`description_jpn`,mi.`title_kor`,mi.`description_kor`,
		mi.`title_spa`,mi.`description_spa`,mi.`title_pol`,mi.`description_pol`,mi.`title_rus`,mi.`description_rus`,mi.`title_tur`,mi.`description_tur`,mi.`title_ron`,mi.`description_ron`,
		mi.`title_dan`,mi.`description_dan`,mi.`title_fin`,mi.`description_fin`,mi.`title_ces`,mi.`description_ces`,mi.`title_hun`,mi.`description_hun`,mi.`title_ara`,mi.`description_ara`
	FROM tmp_dw_upsell_order_items dwoi 
	JOIN tmp_dw_upsell_order_items dw ON dw.order_id = dwoi.order_id
	JOIN menu_items mi ON mi.id = dwoi.`menu_item_id`
	JOIN (	#	get the thumbnail image
		SELECT mu.media_id, mi.id
		FROM menu_items mi
		JOIN media_usage mu ON mi.id = mu.model_id
		JOIN media m ON mu.media_id = m.id
		WHERE  m.name LIKE '%T'
		AND mu.model = 'MenuItem'
	) AS thumb ON thumb.id = dwoi.menu_item_id
	JOIN ( # 	get the landscape image 
		SELECT mu.media_id, mi.id
		FROM menu_items mi
		JOIN media_usage mu ON mi.id = mu.model_id
		JOIN media m ON mu.media_id = m.id
		WHERE  m.name LIKE '%L'
		AND mu.model = 'MenuItem'
	) AS land ON land.id = dwoi.menu_item_id
	JOIN ( #	get the portrait image
		SELECT mu.media_id, mi.id
		FROM menu_items mi
		JOIN media_usage mu ON mi.id = mu.model_id
		JOIN media m ON mu.media_id = m.id
		WHERE  m.name LIKE '%L'
		AND mu.model = 'MenuItem'
	) AS portrait ON portrait.id = dwoi.menu_item_id
	JOIN micros_menu_items mic on mic.id = mi.micros_menu_item_id 	# 	to get the price
	WHERE dw.menu_item_id = ITEMID  
	AND dwoi.menu_item_id <> ITEMID	#	do not include the item sent it
	GROUP BY dwoi.item_title
	ORDER BY item_count DESC LIMIT 6;	
	
	### insert data from temp table into upsell table
	INSERT INTO otg_upsells
	SELECT * from tmp_otg_upsells;
END//
DELIMITER ;

DROP PROCEDURE IF EXISTS sp_get_upsell_ids;
####	get all the ids that are not modifiers
DELIMITER ;;
CREATE PROCEDURE `sp_get_upsell_ids`()
BEGIN
	SELECT DISTINCT mmi.id FROM menu_items mmi 
	LEFT OUTER JOIN
	(	SELECT mi.id, mi.title 
		FROM menu_items mi
		JOIN menu_items_modifier_groups mimg ON mimg.menu_item_id = mi.id
	) mods ON mods.id = mmi.id
	WHERE mods.id IS NULL;
	
END;;
DELIMITER ;

####	get the upsell data from the table based on the item id
DROP PROCEDURE IF EXISTS sp_get_upsells_by_id;
DELIMITER //
CREATE PROCEDURE sp_get_upsells_by_id(ITEMID INT)
BEGIN
	SELECT * 
	FROM otg_upsells otg 
	JOIN menu_items mi on otg.upsell_item_id = mi.id
	WHERE otg.menu_item_id = ITEMID
	AND mi.active = 1;
	
END//
DELIMITER ;


############## view to get all upsell items that are not modifiers, from every menu, per restaurant
DROP VIEW IF EXISTS v_get_upsell_items;
CREATE VIEW v_get_upsell_items
AS
	SELECT DISTINCT mi.id AS menu_item_id, mi.title as item_title, v.id AS menu_id, v.name AS menu_name, v.restaurant_id, v.restaurant_name
	FROM menu_items mi 
	JOIN v_get_menus v ON mi.menu_id = v.id
	LEFT OUTER JOIN menu_items_modifier_groups mimg ON mimg.menu_item_id = mi.id
	WHERE mimg.menu_item_id IS NULL;
	
	
####### procedure to update all default prices where price = 0.00
drop procedure if exists sp_set_default_price_for_upsells;
delimiter //
create procedure sp_set_default_price_for_upsells()
BEGIN
	drop table if exists tmp_price_updates;
	create temporary table tmp_price_updates
	select distinct
		mi.id, mi.title,
		mic.price
	from otg_upsells o
	join menu_items mi on o.upsell_item_id = mi.id
	join modifier_collections mc on mi.modifier_collection_id = mc.id
	join modifier_collections_modifier_groups mcmg on mc.id = mcmg.modifier_collection_id
	join modifier_groups mg on mg.id = mcmg.modifier_group_id
	join menu_items_modifier_groups mimg on mg.id = mimg.modifier_group_id
	join menu_items mmi on mmi.id = mimg.menu_item_id
	join micros_menu_items mic on mic.`id` = mmi.`micros_menu_item_id`
	where o.item_price = 0.00
	and mimg.`selected_by_default` = 1;
	
	update otg_upsells o
	join tmp_price_updates p on o.upsell_item_id = p.id
	set o.item_price = p.price
	where o.item_price = 0.00;

END//
delimiter ;

####	procedure to get the manually entered upsells for an item (when there is no data for that item from the warehouse)
DROP PROCEDURE IF EXISTS sp_setup_manual_upsell_table;
DELIMITER //
CREATE PROCEDURE sp_setup_manual_upsell_table(RESTID int)
BEGIN

	TRUNCATE TABLE tmp_manual_upsell_items;

	INSERT INTO tmp_manual_upsell_items
	SELECT v.menu_item_id, rmi.related_to_menu_item_id
	FROM v_get_upsell_items v
	JOIN related_menu_items rmi ON v.menu_item_id = rmi.menu_item_id
	LEFT OUTER JOIN otg_upsells o ON v.menu_item_id = o.menu_item_id
	WHERE v.restaurant_id = RESTID
	AND o.menu_item_id IS NULL;

	SELECT DISTINCT menu_item_id FROM tmp_manual_upsell_items;
END//
DELIMITER ;

####	get all the data for the manual upsells into the otg_upsells table
DROP PROCEDURE IF EXISTS sp_update_manual_upsells;
DELIMITER //
CREATE PROCEDURE sp_update_manual_upsells(ITEMID int)
BEGIN

	TRUNCATE TABLE tmp_otg_upsells;
	
	INSERT INTO tmp_otg_upsells
	SELECT DISTINCT rmi.menu_item_id, 
		rmi.related_to_menu_item_id AS upsell_item_id, 
		mi.title, mi.description, 
		concat("/images/media/media_",thumb.media_id,".jpg") AS imgUrl,
		concat("/images/media/media_",land.media_id,".jpg") AS detailImageLandscape,
		concat("/images/media/media_",portrait.media_id,".jpg") AS detailImage, 
		mic.price AS item_price, 
		0 AS item_count, 
		mi.modifier_collection_id,
		mi.restrictions,
		now() as create_datetime,
		mi.`title_zhh`,mi.`description_zhh`,mi.`title_fra`,mi.`description_fra`,mi.`title_deu`,mi.`description_deu`,mi.`title_ell`,mi.`description_ell`,mi.`title_heb`,
		mi.`description_heb`,mi.`title_hin`,mi.`description_hin`,mi.`title_ita`,mi.`description_ita`,mi.`title_jpn`,mi.`description_jpn`,mi.`title_kor`,mi.`description_kor`,
		mi.`title_spa`,mi.`description_spa`,mi.`title_pol`,mi.`description_pol`,mi.`title_rus`,mi.`description_rus`,mi.`title_tur`,mi.`description_tur`,mi.`title_ron`,mi.`description_ron`,
		mi.`title_dan`,mi.`description_dan`,mi.`title_fin`,mi.`description_fin`,mi.`title_ces`,mi.`description_ces`,mi.`title_hun`,mi.`description_hun`,mi.`title_ara`,mi.`description_ara`
	FROM tmp_manual_upsell_items rmi 
	JOIN menu_items mi ON mi.id = rmi.related_to_menu_item_id
	JOIN (
		SELECT mu.media_id, mi.id
		FROM menu_items mi
		JOIN media_usage mu ON mi.id = mu.model_id
		JOIN media m ON mu.media_id = m.id
		WHERE  m.name LIKE '%T'
		AND mu.model = 'MenuItem'
	) AS thumb ON thumb.id = rmi.related_to_menu_item_id
	JOIN (
		SELECT mu.media_id, mi.id
		FROM menu_items mi
		JOIN media_usage mu ON mi.id = mu.model_id
		JOIN media m ON mu.media_id = m.id
		WHERE  m.name LIKE '%L'
		AND mu.model = 'MenuItem'
	) AS land ON land.id = rmi.related_to_menu_item_id
	JOIN (
		SELECT mu.media_id, mi.id
		FROM menu_items mi
		JOIN media_usage mu ON mi.id = mu.model_id
		JOIN media m ON mu.media_id = m.id
		WHERE  m.name LIKE '%L'
		AND mu.model = 'MenuItem'
	) AS portrait ON portrait.id = rmi.related_to_menu_item_id
	JOIN micros_menu_items mic on mic.id = mi.micros_menu_item_id
	WHERE rmi.menu_item_id = ITEMID  ;
	
	INSERT INTO otg_upsells
	SELECT * from tmp_otg_upsells;
END//
DELIMITER ;


####### procedure to remove all upsells that are in the same category as the menu_item
DROP PROCEDURE IF EXISTS sp_clean_upsell_categories;
DELIMITER //
CREATE PROCEDURE sp_clean_upsell_categories()
BEGIN
	DELETE
	FROM otg_upsells 
	USING otg_upsells
	JOIN categories_menu_items cmi ON otg_upsells.menu_item_id = cmi.menu_item_id
	JOIN categories_menu_items cmiu ON otg_upsells.upsell_item_id = cmiu.menu_item_id
	WHERE cmi.cat_id = cmiu.cat_id;

END//
DELIMITER ;