import java.util.ArrayList;


public class Happy {

	private int ctr = 0;
	public static void main(String[] args) {
		Happy h = new Happy();
		h.isHappy(20);
	}
	
	public int isHappy(int num) {
		if(ctr++ > 1000) {
			System.out.println("infinite loop. so sad!");
			return 0;
		};
		System.out.println(num);
		int idx = 0;
		ArrayList<Integer> numbers = new ArrayList<Integer>();
		int result = 0;
		char[] nums = new String(""+num).toCharArray();
		for (int i = 0; i < nums.length; i++) {
			int x = Integer.parseInt(""+nums[i]);
			result += (int)(Math.pow(x,2));
		}
		System.out.println("result: "+ result);
		if (result == 1 ) {
			System.out.println("so happy!");
			return 1;
		}
		if ( result == num){
			System.out.println("so sad!");
			return 0;
		}
		return isHappy(result);
	}
}
