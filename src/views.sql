drop view if exists v_get_menus;
CREATE VIEW v_get_menus
AS
SELECT 
	m.id, 
	m.restaurant_id, 
	m.name, 
	m.description, 
	m.micros_id_from, 
	m.micros_id_through, 
	m.template, 
	m.start_time, 
	m.end_time,
	m.notification_plist as plist, 
	m.start_override_datetime as start_override, 
	m.end_override_datetime as end_override, 
	r.name as restaurant_name
FROM menues m
JOIN restaurants r ON m.restaurant_id = r.id
WHERE ABS(timediff(m.end_time,m.start_time)) > 1 ;

drop view if exists v_get_menu_items;
CREATE VIEW v_get_menu_items
AS
SELECT 
	mi.id, 
	`micros_menu_item_id`, 
	`title`, 
	mi.description, 
	`price_override`, 
	`active`, 
	`menu_id`, 
	`modifier_collection_id`, 
	`restrictions`, 
	mi.needs_translation,
	c.id as cat_id,
	c.name as cat_name
FROM menu_items mi
JOIN categories_menu_items cmi ON cmi.menu_item_id = mi.id
JOIN categories c ON c.id = cmi.cat_id;

drop view if exists v_flights;
CREATE VIEW v_flights
AS
SELECT * FROM flights f
WHERE f.Scheduled BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 8 HOUR);

drop view if exists v_dw_orders_order_items;
create view v_dw_orders_order_items
AS
select 
o.order_id,
o.airport,
o.terminal,
o.destination,
o.flight_time,
o.restaurant_id,
o.restaurant_name,
o.table_number,
o.table_zone,
o.order_date,
o.order_time,
o.order_day,
o.order_week,
o.order_month,
o.order_total,
o.micros_payment_type,
o.customer_id,
order_item_id,
menu_item_id,
item_title,
item_price
from dw_orders o
join dw_order_items oi on o.order_id = oi.order_id ;

drop view if exists v_devices;
CREATE VIEW v_devices
AS
SELECT 
	d.id, 
	d.name, 
	d.uid, 
	d.active, 
	d.ClientId as client_id, 
	d.CheckName as check_name, 
	d.TableNumber as table_number 
FROM devices d;

drop view if exists  v_categories;
create view v_categories
as
select distinct 
	c.id,
	c.parent_id,
	c.name,
	c.description,
	c.status,
	c.type,
	c.sort_order,
	c.display_name,
	mi.menu_id 
from categories c
join categories_menu_items cmi on cmi.cat_id = c.id
join menu_items mi on mi.id = cmi.menu_item_id;